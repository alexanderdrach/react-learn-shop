import '../src/styles/styles.scss';
import { Routes, Route } from 'react-router-dom';
import Layout from './conponents/Layout';
import Page from './pages/Page';
import Cart from './pages/Cart';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path='/' element={ <Layout /> }>
          <Route index element={<Page title="Home" type="All"/>}/>
          <Route path="bags" element={<Page title="Bags" type="bag"/>}/>
          <Route path="shoes" element={<Page title="Shoes" type="shoes"/>}/>
          <Route path="hats" element={<Page title="Hats" type="hat"/>}/>
          <Route path="cart" element={<Cart />}/>
        </Route>
      </Routes>
    </div>
  );
}

export default App;
