import TitlePage from '../conponents/TitlePage';
import Button from '../conponents/Button';
import CartCard from '../conponents/CartCard';
import HeaderPage from '../conponents/HeaderPage';
import { useSelector } from 'react-redux';

const Cart = () => {
  const cartProducts = useSelector(state => state.cartProducts.cartProducts);
  let totalPrice = cartProducts.reduce((sum, item) => sum + (item.price * item.quantity), 0)
  return (
    <>
      <HeaderPage >
        <TitlePage title="Cart"/>
      </HeaderPage>
      <div className="cart-content">
        <div className="cart-header">
          <h3 className='cart-title cart-title_product'>Product</h3>
          <h3 className='cart-title cart-title_price'>Price</h3>
          <h3 className='cart-title cart-title_quantity'>Quantity</h3>
        </div>
        <div className="cart-card-list">
          {cartProducts.length > 0 ? cartProducts.map(item => {
            return (
              <CartCard key={item.id} id={item.id} thumb={item.thumb} title={item.title} price={item.price} quantity={item.quantity}/>
            )
          }) : 'корзина пуста'}
        </div>

        <div className='total-price'>
          <span>{totalPrice > 0 ? `Total ${totalPrice} $` : ''}</span>
        </div>
      </div>
      <Button className={'btn btn_primary justify-end'} text="checkout"/>
    </>
  )
}

export default Cart;