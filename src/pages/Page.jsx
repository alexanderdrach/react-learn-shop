import TitlePage from '../conponents/TitlePage';
import ProductList from '../conponents/ProductList';
import HeaderPage from '../conponents/HeaderPage';
import Filter from '../conponents/Filter';
import Button from '../conponents/Button';
import { useState } from 'react';
import { useSelector } from 'react-redux';

const Page = ({title, type}) => {
  const [directionSort, setDirectionSort] = useState('');
  const [parameterFilter, setPparameterFilter] = useState('');
  const [countItems, setcountItems] = useState(15);
  const products = useSelector(state => state.products.products);
  let productsLength = products.length;

  return (
    <>
      <HeaderPage>
        <TitlePage title={title}/>
        <Filter
          setDirectionSort={setDirectionSort}
          setPparameterFilter={setPparameterFilter}
        />
      </HeaderPage>
      <ProductList
        type={type}
        directionSort={directionSort}
        parameterFilter={parameterFilter}
        countItems={countItems}
      />
      {countItems >= productsLength ?
      <Button onClick={() => {setcountItems(15)}} className={'btn btn_primary justify-center'} text="show less"/>
      :
      <Button onClick={() => {setcountItems(countItems + 5)}} className={'btn btn_primary justify-center'} text="show more"/>
      }
    </>
  )
}

export default Page;