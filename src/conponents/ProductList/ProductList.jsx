import './product-list.scss';
import { useSelector } from 'react-redux';
import Card from '../Card';

const ProductList = ({type, directionSort, parameterFilter, countItems}) => {
  const products = useSelector(state => state.products.products);

  let arr = products;
  if (type !== 'All'){
    arr = products.filter(item => item.type === type);
  } else {
    arr = products;
  }

  if (parameterFilter) {
    arr = arr.filter(item => item.color === parameterFilter);
  }

  if (directionSort === 'up') {
    arr = [...arr].sort( (a, b) => {
      return a.price - b.price
    })
  }

  if (directionSort === 'down') {
    arr = [...arr].sort( (a, b) => {
      return b.price - a.price
    })
  }

  const sliceArr = arr.slice(0, countItems);

  return (
    <div className='product-list'>
      {sliceArr.map( (item) => {
        return (
          <Card id={item.id} title={item.name} price={item.price} key={item.id} thumb={item.thumb}/>
        )
      })}
    </div>
  )
}

export default ProductList;