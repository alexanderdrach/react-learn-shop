import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import './header.scss';
import logo from '../../assets/images/logo.svg';

const Header = (props) => {
  const cartProducts = useSelector(state => state.cartProducts.cartProducts);
  let quantityItems = cartProducts.reduce((summ, item) => summ + item.quantity, 0);
  let totalPrice = cartProducts.reduce((sum, item) => sum + (item.price * item.quantity), 0);

  return (
    <header className="header">
      <div className="container">
        <div className="header__inner">

          <Link to="/" className="logo">
            <img src={logo} alt="logo"/>
          </Link>

          <nav className="nav">
            <Link to="/bags" className="nav__item" href="#">bags</Link>
            <Link to="/shoes" className="nav__item" href="#">shoes</Link>
            <Link to="/hats" className="nav__item" href="#">hats</Link>
          </nav>

          <div className="cart">
            <div className="cart__count">
              <span className="cart__quantity">{quantityItems}</span>
              <span>items</span>
            </div>
            <div className="cart__cost">
              <span>{totalPrice}</span>
              <span>$</span>
            </div>
            <Link to="/cart" className="cart__link" href="#"></Link>
          </div>

        </div>
      </div>
    </header>
  )
}

export default Header;