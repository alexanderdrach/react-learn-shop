import './card.scss';
import { useDispatch } from 'react-redux';
import { addProductToCart } from '../../store/cartSlice';

const Card = ({id, type, thumb, title, price}) => {
  const dispatch = useDispatch()
  return (
    <div className='card' onClick={() => dispatch(addProductToCart({id, thumb, title, price}))}>
      <div className="card__thumb">
        { thumb ? <img src={thumb} alt=""/> : 'нет изображения' }
      </div>
      <h3 className='card__title'>{title}</h3>
      <p className='card__price'>{price}$</p>
    </div>
  )
}

export default Card;