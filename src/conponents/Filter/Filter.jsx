import './filter.scss';
import Button from '../Button';

const Filter = ({setDirectionSort, setPparameterFilter}) => {
  return (
    <div className="filter-block">
      <div className="sort-price">
        <span className="filter-label">Sort by price</span>
        <Button className={'btn sort'} text="&uarr;" onClick={() => setDirectionSort('up')}/>
        <Button className={'btn sort'} text="&darr;" onClick={() => setDirectionSort('down')}/>
        <Button className={'btn reset'} onClick={() => setDirectionSort('')} text="Reset sort"/>
      </div>
      <div className="filter-color">
        <span className="filter-label">Filter by color</span>
        <Button className={'btn filter white'} onClick={() => setPparameterFilter('white')}/>
        <Button className={'btn filter black'} onClick={() => setPparameterFilter('black')}/>
        <Button className={'btn filter pink'} onClick={() => setPparameterFilter('pink')}/>
        <Button className={'btn filter red'} onClick={() => setPparameterFilter('red')}/>
        <Button className={'btn filter green'} onClick={() => setPparameterFilter('green')}/>
        <Button className={'btn reset'} onClick={() => setPparameterFilter('')} text="Reset filter"/>
      </div>
    </div>
  )
}

export default Filter;