import './card-card.scss';
import thumb from '../../assets/images/img-bug.jpg';
import { useDispatch } from 'react-redux';
import { removeProductInCart } from '../../store/cartSlice';
import { useSelector } from 'react-redux';

const CartCard = ({id, thumb, title, price, quantity}) => {
  const dispatch = useDispatch();
  const cartProducts = useSelector(state => state.cartProducts.cartProducts);
  return (
    <div className="cart-card">
      <div className="cart-card__thumb-title-wrapper">
        <div className="cart-card__thumb">
          <img src={thumb} alt="" />
        </div>
        <h3 className='cart-card__title'>{title}</h3>
      </div>
      <div className="cart-card__price">
        <div>{quantity * price}$</div>
        <div className="cart-card__item-cost">{price}$ per item</div>
      </div>
      <div className="cart-card__quantity">
        <span>{quantity}</span>
      </div>
      <button className="btn-delete" type="button" onClick={() => dispatch(removeProductInCart({id}))}>delete</button>
    </div>
  )
}

export default CartCard;