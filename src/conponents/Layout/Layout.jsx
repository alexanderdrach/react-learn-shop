import Header from "../Header";
import { Outlet } from "react-router-dom";
import './layout.scss';

const Button = (props) => {
  return (
    <>
    <Header />
    <main className="main">
      <div className="container">
        <div className="main__inner">
          <Outlet />
        </div>
      </div>
    </main>
    </>
  )
}

export default Button;