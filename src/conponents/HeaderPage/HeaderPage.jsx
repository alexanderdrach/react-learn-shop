import './header-page.scss';

const HeaderPage = (props) => {
  return (
    <div className="block-title-filter">
      {props.children}
    </div>
  )
}

export default HeaderPage;