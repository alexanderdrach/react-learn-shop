import {createSlice, current } from '@reduxjs/toolkit';
import thumbBag from '../assets/images/img-bug.jpg';
import thumbBag1 from '../assets/images/white-bag-1.jpeg';
import thumbBag2 from '../assets/images/white-bag-2.jpg';
import thumbBag3 from '../assets/images/white-bag-3.jpg';
import thumbBag4 from '../assets/images/red-bag-1.jpg';
import thumbBag5 from '../assets/images/red-bag-2.jpg';
import thumbBag6 from '../assets/images/green-bag-1.jpg';
import thambHat from '../assets/images/img-hut.jpg';
import thambHat1 from '../assets/images/rad-hat-1.jpg';
import thambHat2 from '../assets/images/green-hat-1.jpg';
import thambShoes from '../assets/images/img-shoes.jpeg';
import thambShoes1 from '../assets/images/green-shoes-1.jpg'
import thambShoes2 from '../assets/images/red-shoes-1.jpeg'

const productsSlice = createSlice({
  name: 'products',
  initialState: {
    products: [
      {
        id: 'bag-1',
        type: 'bag',
        thumb: thumbBag,
        color: 'pink',
        name: 'bag-1',
        price: 100,
      },
      {
        id: 'bag-10',
        type: 'bag',
        thumb: thumbBag4,
        color: 'red',
        name: 'bag-10',
        price: 100,
      },
      {
        id: 'bag-2',
        type: 'bag',
        thumb: thumbBag1,
        color: 'white',
        name: 'bag-2',
        price: 200
      },
      {
        id: 'bag-3',
        type: 'bag',
        // thumb: thumbBag,
        color: '',
        name: 'bag-3',
        price: 150
      },
      {
        id: 'bag-9',
        type: 'bag',
        thumb: thumbBag4,
        color: 'red',
        name: 'bag-9',
        price: 496
      },
      {
        id: 'bag-5',
        type: 'bag',
        thumb: thumbBag,
        color: 'pink',
        name: 'bag-5',
        price: 56
      },
      {
        id: 'bag-6',
        type: 'bag',
        thumb: thumbBag2,
        color: 'white',
        name: 'bag-6',
        price: 42
      },
      {
        id: 'bag-8',
        type: 'bag',
        thumb: thumbBag3,
        color: 'white',
        name: 'bag-8',
        price: 42
      },
      {
        id: 'bag-7',
        type: 'bag',
        thumb: thumbBag,
        color: 'pink',
        name: 'bag-7',
        price: 1500
      },
      {
        id: 'bag-12',
        type: 'bag',
        thumb: thumbBag5,
        color: 'red',
        name: 'bag-12',
        price: 540
      },
      {
        id: 'bag-11',
        type: 'bag',
        thumb: thumbBag6,
        color: 'green',
        name: 'bag-11',
        price: 540
      },
      {
        id: 'hat-1',
        type: 'hat',
        thumb: thambHat,
        color: 'black',
        name: 'hat-1',
        price: 100,
      },
      {
        id: 'hat-2',
        type: 'hat',
        thumb: thambHat1,
        color: 'red',
        name: 'hat-2',
        price: 200
      },
      {
        id: 'hat-3',
        type: 'hat',
        thumb: thambHat,
        color: 'black',
        name: 'hat-3',
        price: 150
      },
      {
        id: 'hat-4',
        type: 'hat',
        thumb: thambHat2,
        color: 'green',
        name: 'hat-4',
        price: 88
      },
      {
        id: 'hat-5',
        type: 'hat',
        thumb: thambHat,
        color: 'black',
        name: 'hat-5',
        price: 90
      },
      {
        id: 'hat-6',
        type: 'hat',
        thumb: thambHat,
        color: 'black',
        name: 'hat-6',
        price: 320
      },
      {
        id: 'hat-7',
        type: 'hat',
        thumb: thambHat,
        color: 'black',
        name: 'hat-7',
        price: 450
      },
      {
        id: 'hat-8',
        type: 'hat',
        thumb: thambHat,
        color: 'black',
        name: 'hat-8',
        price: 600
      },
      {
        id: 'shoes-1',
        type: 'shoes',
        thumb: thambShoes,
        color: 'pink',
        name: 'shoes-1',
        price: 100,
      },
      {
        id: 'shoes-2',
        type: 'shoes',
        thumb: thambShoes,
        color: 'pink',
        name: 'shoes-2',
        price: 200
      },
      {
        id: 'shoes-3',
        type: 'shoes',
        thumb: thambShoes,
        color: 'pink',
        name: 'shoes-3',
        price: 150
      },
      {
        id: 'shoes-4',
        type: 'shoes',
        thumb: thambShoes,
        color: 'pink',
        name: 'shoes-4',
        price: 95
      },
      {
        id: 'shoes-5',
        type: 'shoes',
        thumb: thambShoes1,
        color: 'green',
        name: 'shoes-5',
        price: 3200
      },
      {
        id: 'shoes-6',
        type: 'shoes',
        thumb: thambShoes,
        color: 'pink',
        name: 'shoes-6',
        price: 70
      },
      {
        id: 'shoes-7',
        type: 'shoes',
        thumb: thambShoes,
        color: 'pink',
        name: 'shoes-7',
        price: 38
      },
      {
        id: 'shoes-8',
        type: 'shoes',
        thumb: thambShoes2,
        color: 'red',
        name: 'shoes-8',
        price: 250
      }
    ],
  },
  reducers: {
  }
});

export default productsSlice.reducer;