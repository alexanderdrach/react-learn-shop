import {createSlice } from '@reduxjs/toolkit';

const cartSlice = createSlice({
  name: 'cartProducts',
  initialState: {
    cartProducts: [],
  },
  reducers: {
    addProductToCart(state, action) {
      let indexElement = state.cartProducts.findIndex((item) => item.id === action.payload.id);
      if(indexElement !== -1) {
        state.cartProducts[indexElement].quantity = state.cartProducts[indexElement].quantity + 1
      } else {
        state.cartProducts = [...state.cartProducts, {
          id: action.payload.id,
          thumb: action.payload.thumb,
          title: action.payload.title,
          price: action.payload.price,
          quantity: 1,
        }]
      }
    },
    removeProductInCart(state, action) {
      state.cartProducts = state.cartProducts.filter(item => item.id !== action.payload.id);
    }
  }
});

export const { addProductToCart, removeProductInCart } = cartSlice.actions;
export default cartSlice.reducer;